# EPEL package repository

This Ansible role installs the EPEL package repository.

When run, it does the following:
1. installs the repository and its signing key
2. disables the repository by default so it does not unintentionally take
   precedence over Red Hat or UVM packages
3. preconfigures a list of excluded packages so they do not interfere with
   locally-built packages
4. optionally installs a list of EPEL packages

## Installation

Add the following to a `requirements.yml` or `requirements.yaml` file in your
Ansible project and run `ansible-galaxy install -r requirements.yaml`:

```yaml
roles:
  - name: epel
    scm: git
    src: https://gitlab.uvm.edu/saa/ansible-galaxy/epel.git
```


## Usage

Place a section like this in your playbook:

```yaml
- name: Set up EPEL and packages
  hosts: all
  roles:
    - role: epel
      install_packages:
        - neovim
        - pwgen
```
### Additional options

Full argument documentation is available via `ansible-doc -t role epel`.
